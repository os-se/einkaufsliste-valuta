# Lebenslauf

## Persönliche Angaben
Name: Max Mustermann

Geburtsdatum: 01.01.1990

Geburtsort: Berlin

Adresse: Musterstraße 1, 12345 Musterstadt

Telefon: 0123456789

E-Mail: max.mustermann@mustermail.de

## Schulbildung
2000-2009: Grundschule Musterstadt

2009-2012: Gymnasium Musterstadt

2012-2015: Abitur am Gymnasium Musterstadt

Berufserfahrung
2015-2016: Praktikum bei Musterfirma GmbH

2016-2018: Ausbildung zum Industriekaufmann bei Musterfirma GmbH

2018-2020: Industriekaufmann bei Musterfirma GmbH

## Kenntnisse und Fähigkeiten
Microsoft Office (Word, Excel, PowerPoint)
SAP
Englisch (fließend)
